#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <iostream>

#include "Employee.hpp"
#include "IDatabaseConnection.hpp"
#include "EmployeeManager.hpp"

#include <functional>

using namespace testing;

class MockDatabaseConnection : public IDatabaseConnection
{
public:
    MockDatabaseConnection(std::string serverAddress);

    MOCK_METHOD0(connect, void());
    MOCK_METHOD0(disconnect, void());

    MOCK_CONST_METHOD1(getSalary, float(int));
    MOCK_METHOD2(updateSalary, void(int, float));

    MOCK_CONST_METHOD1(getSalariesRange, std::vector<Employee>(float));
    MOCK_CONST_METHOD2(getSalariesRange, std::vector<Employee>(float, float));

    void someMemberMethodAccessedByTestCase()
    {
        std::cout << "Member method called without parameter \n";
        throw std::runtime_error("Dummy error!");
    }

    void someMemberMethodWithParametersAccessedByTestCase(std::string param1)
    {
        std::cout << "Member method called with parameter :" << param1 << std::endl;
        throw std::runtime_error("Dummy error!");
    }

};

MockDatabaseConnection::MockDatabaseConnection(std::string serverAddress)
:
IDatabaseConnection(serverAddress)
{

}

/*
It is way to to tell Google Mock that what happens when we attempt to call 
a specific mocked method and it needs to throw an exception 

*/

//Approach 1/5 - Use .WillOnce() to throw exception
TEST(TestEmployeeManager, TestConnectionError1) // Simulate Connection Error
{
    MockDatabaseConnection mdc1("DummyAddress");
    
    EXPECT_CALL(mdc1, connect()).WillOnce(Throw(std::runtime_error("Dummy Error")));

    /*
    //EXPECT on disconnect() is not required as connect() itself is failing
    //EXPECT_CALL(mdc1, disconnect());
    */

   /*
    Since em1(&mdc1) will call constructer and in turn will call connect()
    Which now would end in exception, so dont keep in bare call use
    EXPECT_THROW() on 
    EmployeeManager em1(&mdc1);
   */
    EXPECT_THROW(EmployeeManager em1(&mdc1), std::runtime_error);
    // EmployeeManager em1(&mdc1);
}

//Approach 2/5 - Using ACTION() method to define all sub calls / throw

ACTION(myActionName)
{
    std::cout << "Throwing an error!\n";
    throw std::runtime_error("Dummy Error");
}

TEST(TestEmployeeManager, TestConnectionError2) // Simulate Connection Error
{
    MockDatabaseConnection mdc1("DummyAddress");
    EXPECT_CALL(mdc1, connect()).WillOnce(myActionName());
    EXPECT_THROW(EmployeeManager em1(&mdc1), std::runtime_error);
}

//Approach 3/5 - Using any free function and then Invoking it

void someFreeFunction()
{
    std::cout << "Throwing an error in free function!\n";
    throw std::runtime_error("Dummy Error");
}

TEST(TestEmployeeManager, TestConnectionError3) // Simulate Connection Error
{
    MockDatabaseConnection mdc1("DummyAddress");
    EXPECT_CALL(mdc1, connect()).WillOnce(Invoke(someFreeFunction));
    EXPECT_THROW(EmployeeManager em1(&mdc1), std::runtime_error);
}

//Approach 4/5 - Using Lambda function while invoking it

TEST(TestEmployeeManager, TestConnectionError4) // Simulate Connection Error
{
    MockDatabaseConnection mdc1("DummyAddress");
    EXPECT_CALL(mdc1, connect()).WillOnce(Invoke(
        [](){
            std::cout << " In lambda..\n";
            throw std::runtime_error("Dummy error");
        }
    ));
    EXPECT_THROW(EmployeeManager em1(&mdc1), std::runtime_error);
}

//Approach 5/5 - When forced to use member function - Without Parameters
//Use bind'ing to bind member function to testcase

TEST(TestEmployeeManager, TestConnectionError5_1) // Simulate Connection Error
{
    MockDatabaseConnection mdc1("DummyAddress");

    // std::bind requires #include <functional>
    auto boundMethod = std::bind(&MockDatabaseConnection::someMemberMethodAccessedByTestCase, &mdc1);

    //Arrange + ACT
    EXPECT_CALL(mdc1, connect()).WillOnce(InvokeWithoutArgs(boundMethod));

    //ASSERT
    EXPECT_THROW(EmployeeManager em1(&mdc1), std::runtime_error);
}

// When forced to use member function - With Parameters 

TEST(TestEmployeeManager, TestConnectionError5_2) // Simulate Connection Error
{
    MockDatabaseConnection mdc1("DummyAddress");

    // std::bind requires #include <functional>
    auto boundMethod = std::bind(&MockDatabaseConnection::someMemberMethodWithParametersAccessedByTestCase, &mdc1, "PeterParker");

    //Arrange + ACT
    EXPECT_CALL(mdc1, connect()).WillOnce(InvokeWithoutArgs(boundMethod));

    //ASSERT
    EXPECT_THROW(EmployeeManager em1(&mdc1), std::runtime_error);
}

int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}