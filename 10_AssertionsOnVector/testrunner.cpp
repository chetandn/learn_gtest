#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include "GenerateNumber.hpp"

using namespace testing;


TEST(VectorTests, ElementsGenereatedTest) 
{
    std::vector<int> v1 = generateNumber(5, 3); // this should generate 0,1,2,0,1

    EXPECT_THAT(v1, Each(AllOf(Ge(0), Lt(3))));
}

int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}