#pragma once

#include <vector>

/*
Generate {n} number of {n}%{limit}
*/
std::vector<int> generateNumber(int n, int limit);
