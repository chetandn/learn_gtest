#include <gtest/gtest.h>
#include "CheckPositives.hpp"
#include <vector>

TEST(TestSuiteCountPositives, basicTest)
{
    // Step 1: Arrange
    std::vector<int> inputVector{1, -2, -3, 3, -4, -5, 7};

    // Step 2: Act
    int count = countPositives(inputVector);

    // Step 3: Assert
    EXPECT_EQ(3, count);

}

TEST(TestSuiteCountPositives, emptyVectorTest)
{
    // Step 1: Arrange
    std::vector<int> inputVector{};

    // Step 2: Act
    int count = countPositives(inputVector);

    // Step 3: Assert
    EXPECT_EQ(0, count);
}

TEST(TestSuiteCountPositives, allNegativeTest)
{
    // Step 1: Arrange
    std::vector<int> inputVector{-2, -1, -55};

    // Step 2: Act
    int count = countPositives(inputVector);

    // Step 3: Assert
    EXPECT_EQ(0, count);
}

int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}