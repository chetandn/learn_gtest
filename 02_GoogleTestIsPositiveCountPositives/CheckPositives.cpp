#include "CheckPositives.hpp"
#include <algorithm>

bool isPositive(int num)
{
    return num>=0;
}

int countPositives(std::vector<int> const& inputVector)
{
    return std::count_if(inputVector.begin(), inputVector.end(), isPositive);
}