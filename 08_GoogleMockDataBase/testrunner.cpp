#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <iostream>

#include "Employee.hpp"
#include "IDatabaseConnection.hpp"
#include "EmployeeManager.hpp"

using namespace testing;

class MockDatabaseConnection : public IDatabaseConnection
{
public:
    MockDatabaseConnection(std::string serverAddress);

    MOCK_METHOD0(connect, void());
    MOCK_METHOD0(disconnect, void());

    MOCK_CONST_METHOD1(getSalary, float(int));
    MOCK_METHOD2(updateSalary, void(int, float));

    MOCK_CONST_METHOD1(getSalariesRange, std::vector<Employee>(float));
    MOCK_CONST_METHOD2(getSalariesRange, std::vector<Employee>(float, float));

};

MockDatabaseConnection::MockDatabaseConnection(std::string serverAddress)
:
IDatabaseConnection(serverAddress)
{

}

TEST(TestEmployeeManager, TestConnection)
{
    MockDatabaseConnection mdc1("dummyServerAddress");

    EXPECT_CALL(mdc1, connect());
    EXPECT_CALL(mdc1, disconnect());

    // mdc1.connect();
    // mdc1.disconnect();
    EmployeeManager em1(&mdc1);

}

TEST(TestEmployeeManager, TestSetSalary)
{
    MockDatabaseConnection mdc1("dummyServerAddress");

    EXPECT_CALL(mdc1, connect());
    EXPECT_CALL(mdc1, updateSalary(_, _)).Times(1);
    EXPECT_CALL(mdc1, disconnect());

    EmployeeManager em1(&mdc1);
    em1.setSalary(123, 295789);
}

TEST(TestEmployeeManager, TesGetSalary)
{

    const int empId = 50;
    const float salary = 6100.5;

    MockDatabaseConnection mdc1("dummyServerAddress");

    EXPECT_CALL(mdc1, connect());
    EXPECT_CALL(mdc1, getSalary(empId)).Times(1).WillOnce(Return(salary));
    EXPECT_CALL(mdc1, disconnect());

    EmployeeManager em1(&mdc1);
    float returnedSalary = em1.getSalary(empId);

    EXPECT_EQ(salary, returnedSalary);

}

TEST(TestEmployeeManager, TesGetSalaryRange)
{

    const int low = 5000;
    const int high = 8000;

    std::vector<Employee> returnedVector{
                                            Employee{1, 5000, "Ram"},
                                            Employee{25, 7654, "Mohan"},
                                            Employee{7, 6734, "Dia"}
                                        };


    MockDatabaseConnection mdc1("dummyServerAddress");

    EXPECT_CALL(mdc1, connect());
    EXPECT_CALL(mdc1, disconnect());
    EXPECT_CALL(mdc1, getSalariesRange(_,_)).WillOnce(Return(returnedVector));

    EmployeeManager em1(&mdc1);
    std::map<std::string, float> returnedMap = em1.getSalariesBetween(low, high);

    for(auto ele : returnedMap)
    {
        std::cout << ele.first << " " << ele.second <<'\n';
    }



}

int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}