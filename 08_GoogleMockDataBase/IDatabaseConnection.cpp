#include "IDatabaseConnection.hpp"
#include <iostream>

IDatabaseConnection::IDatabaseConnection(std::string serverAddress)
:
mServerAddress{serverAddress}
{
}

void IDatabaseConnection::connect()
{
    std::cout << "Connecting to database server : " << mServerAddress << std::endl;
}

void IDatabaseConnection::disconnect()
{
    std::cout << "Disconnecting from database" << std::endl;
}