#include <gtest/gtest.h>
#include "Validator.hpp"
#include <iostream>


// Step 1: Make a fixture class
class ValidatorFixture : public testing::TestWithParam<int>
{
public:
protected:
    Validator val1{5,10};
};


// Step 2: Test body of Parameterized Fixture 
TEST_P(ValidatorFixture, checkInrange)
{
    int param = GetParam();
    std::cout << "Param :" << param << '\n';

    bool isInside = val1.inRange(param);

    EXPECT_TRUE(isInside);
}

// Step 3: Create TEst suite
// INSTANTIATE_TEST_CASE_P(prefix, test_case_name, generator)
// INSTANTIATE_TEST_CASE_P(InRangeTrue, ValidatorFixture, testing::Values(5,6,7,8,9,10));      // 6 tests from 1 test case ran. (0 ms total) | [  PASSED  ] 6 tests.

INSTANTIATE_TEST_CASE_P(InRangeTrue, ValidatorFixture, testing::Values(5,6,7,8,9,10,20));   //

// [==========] 7 tests from 1 test case ran. (1 ms total)
// [  PASSED  ] 6 tests.
// [  FAILED  ] 1 test, listed below:
// [  FAILED  ] InRangeTrue/ValidatorFixture.checkInrange/6, where GetParam() = 20



int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}