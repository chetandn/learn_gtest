#include <gtest/gtest.h>
#include "Validator.hpp"
#include <iostream>


// Step 1: Make a fixture class
class ValidatorFixture : public testing::TestWithParam<int>
{
public:
protected:
    Validator val1{5,10};
};


// Step 2: Test body of Parameterized Fixture 
TEST_P(ValidatorFixture, checkOutOfrange)
{
    int param = GetParam();
    std::cout << "Param :" << param << '\n';

    bool isInside = val1.inRange(param);

    EXPECT_FALSE(isInside);
}

// Step 3: Create TEst suite
INSTANTIATE_TEST_CASE_P(InRangeFalse, ValidatorFixture, testing::Values(-200, 100, 3, 0, 15));  


//---Output----
//[==========] Running 5 tests from 1 test case.
// [----------] Global test environment set-up.
// [----------] 5 tests from InRangeFalse/ValidatorFixture
// [ RUN      ] InRangeFalse/ValidatorFixture.checkOutOfrange/0
// Param :-200
// [       OK ] InRangeFalse/ValidatorFixture.checkOutOfrange/0 (0 ms)
// [ RUN      ] InRangeFalse/ValidatorFixture.checkOutOfrange/1
// Param :100
// [       OK ] InRangeFalse/ValidatorFixture.checkOutOfrange/1 (0 ms)
// [ RUN      ] InRangeFalse/ValidatorFixture.checkOutOfrange/2
// Param :3
// [       OK ] InRangeFalse/ValidatorFixture.checkOutOfrange/2 (0 ms)
// [ RUN      ] InRangeFalse/ValidatorFixture.checkOutOfrange/3
// Param :0
// [       OK ] InRangeFalse/ValidatorFixture.checkOutOfrange/3 (0 ms)
// [ RUN      ] InRangeFalse/ValidatorFixture.checkOutOfrange/4
// Param :15
// [       OK ] InRangeFalse/ValidatorFixture.checkOutOfrange/4 (0 ms)
// [----------] 5 tests from InRangeFalse/ValidatorFixture (0 ms total)

// [----------] Global test environment tear-down
// [==========] 5 tests from 1 test case ran. (0 ms total)
// [  PASSED  ] 5 tests.


int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}