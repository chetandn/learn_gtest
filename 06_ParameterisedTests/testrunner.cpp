#include <gtest/gtest.h>
#include "Validator.hpp"
#include <iostream>


// Step 1: Make a fixture class
class ValidatorFixture : public testing::TestWithParam<std::tuple<int, bool>>
{
public:
protected:
    Validator val1{5,10};
};


// Step 2: Test body of Parameterized Fixture 
TEST_P(ValidatorFixture, checkInAndOutOfRangeTogether)
{
    std::tuple<int, bool> tpl1 = GetParam();
    int param = std::get<0>(tpl1);
    int expectedResult = std::get<1>(tpl1);

    std::cout << "Param :" << param << '\n';
    std::cout << "expectedResult :" << expectedResult << '\n';

    bool isInside = val1.inRange(param);

    EXPECT_EQ(expectedResult, isInside);
}

// Step 3: Create TEst suite
INSTANTIATE_TEST_CASE_P(InRangeTest, ValidatorFixture, testing::Values(
                                                                        std::make_tuple(-50,false),
                                                                        std::make_tuple(4,false),
                                                                        std::make_tuple(5,true),
                                                                        std::make_tuple(7,true),
                                                                        std::make_tuple(10,true),
                                                                        std::make_tuple(11,false),
                                                                        std::make_tuple(200,false)                                                                        
                                                                        )                        
);  

//----------------output--------------------
/*
[==========] Running 7 tests from 1 test case.
[----------] Global test environment set-up.
[----------] 7 tests from InRangeTest/ValidatorFixture
[ RUN      ] InRangeTest/ValidatorFixture.checkInAndOutOfRangeTogether/0
Param :-50
expectedResult :0
[       OK ] InRangeTest/ValidatorFixture.checkInAndOutOfRangeTogether/0 (0 ms)
[ RUN      ] InRangeTest/ValidatorFixture.checkInAndOutOfRangeTogether/1
Param :4
expectedResult :0
[       OK ] InRangeTest/ValidatorFixture.checkInAndOutOfRangeTogether/1 (0 ms)
[ RUN      ] InRangeTest/ValidatorFixture.checkInAndOutOfRangeTogether/2
Param :5
expectedResult :1
[       OK ] InRangeTest/ValidatorFixture.checkInAndOutOfRangeTogether/2 (0 ms)
[ RUN      ] InRangeTest/ValidatorFixture.checkInAndOutOfRangeTogether/3
Param :7
expectedResult :1
[       OK ] InRangeTest/ValidatorFixture.checkInAndOutOfRangeTogether/3 (0 ms)
[ RUN      ] InRangeTest/ValidatorFixture.checkInAndOutOfRangeTogether/4
Param :10
expectedResult :1
[       OK ] InRangeTest/ValidatorFixture.checkInAndOutOfRangeTogether/4 (0 ms)
[ RUN      ] InRangeTest/ValidatorFixture.checkInAndOutOfRangeTogether/5
Param :11
expectedResult :0
[       OK ] InRangeTest/ValidatorFixture.checkInAndOutOfRangeTogether/5 (0 ms)
[ RUN      ] InRangeTest/ValidatorFixture.checkInAndOutOfRangeTogether/6
Param :200
expectedResult :0
[       OK ] InRangeTest/ValidatorFixture.checkInAndOutOfRangeTogether/6 (0 ms)
[----------] 7 tests from InRangeTest/ValidatorFixture (0 ms total)

[----------] Global test environment tear-down
[==========] 7 tests from 1 test case ran. (5 ms total)
[  PASSED  ] 7 tests.

*/
//------------------------------------------




int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}