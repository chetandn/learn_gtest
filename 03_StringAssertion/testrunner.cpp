#include <gtest/gtest.h>
#include "StringFormat.hpp"
#include <iostream>

TEST(TestSuiteCountPositives, basicTest)
{
    // Step 1: Arrange
    char inputString[] = "Hello World";

    // Step 2: Act
    toUpper(inputString);

    // Step 3: Assert
    //EXPECT_EQ("HELLO WORLD", inputString); 
    /*
    *   Value of: inputString
    *   Actual: 0x7fff8a936e10
    *   Expected: "HELLO WORLD"
    *   Which is: 0x46c6e5
    */

   EXPECT_STREQ("HELLO WORLD", inputString);
   EXPECT_STRCASEEQ("HeLlO wORLd", inputString); //Ignore case

}



int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}