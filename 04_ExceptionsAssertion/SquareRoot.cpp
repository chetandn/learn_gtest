#include "SquareRoot.hpp"
#include <math.h>
#include <stdexcept>


double getSqrt(double num)
{
	if(num<0)
	{
		throw std::runtime_error("Negative arg!");
	}
	return sqrt(num);
}
