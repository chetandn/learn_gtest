#include <gtest/gtest.h>
#include "SquareRoot.hpp"
#include <iostream>

TEST(TestSuiteCountPositives, basicTest)
{
    // Step 1: Arrange
    double inputValue = 9;

    // Step 2: Act
    double res = getSqrt(inputValue);

    // Step 3: Assert
    EXPECT_EQ(3, res);

}

TEST(TestSuiteCountPositives, negativeArgTest)
{
    // Step 1: Arrange
    double inputValue = -9;

    // Step 2: Act
    // double res = getSqrt(inputValue);

    // Step 3: Assert
    // EXPECT_ANY_THROW(getSqrt(inputValue)); or

    // To get in specific
    // EXPECT_THROW(getSqrt(inputValue), std::overflow_error);
    EXPECT_THROW(getSqrt(inputValue), std::runtime_error);

}

TEST(TestSuiteCountPositives, positiveArgTest)
{
    // Step 1: Arrange
    double inputValue = 16;

    // Step 2: Act
    // double res = getSqrt(inputValue);

    // Step 3: Assert
    EXPECT_NO_THROW(getSqrt(inputValue));

}



int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}