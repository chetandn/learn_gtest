#include <gtest/gtest.h>
#include "BankAccount.hpp"
#include <iostream>

TEST(AccountTest, TestEmptyAccount)
{
    // Step 1: Arrange
    Account acc1;

    // Step 2: Act
    double balance = acc1.getBalance();

    // Step 3: Assert
    EXPECT_EQ(0, balance);

}

TEST(AccountTest, TestDeposit)
{
    // Step 1: Arrange
    Account acc1;

    // Step 2: Act
    acc1.deposit(20.33);

    // Step 3: Assert
    EXPECT_EQ(20.33, acc1.getBalance());

}

TEST(AccountTest, TestWithdraw)
{
    // Step 1: Arrange
    Account acc1;
    acc1.deposit(50.66);

    // Step 2: Act
    acc1.withdraw(20.33);

    // Step 3: Assert
    EXPECT_EQ(30.33, acc1.getBalance());

}

TEST(AccountTest, TestWithdrawInsufficientFund)
{
    // Step 1: Arrange
    Account acc1;
    acc1.deposit(50.66);

    // Step 3: Assert
    EXPECT_THROW(acc1.withdraw(200.33), std::runtime_error);

}

TEST(AccountTest, transferIsSucccessful)
{

    Account acc1;
    acc1.deposit(50.66);

    Account acc2;

    acc1.transfer(acc2, 5.11);

    EXPECT_EQ(45.55, acc1.getBalance());
    EXPECT_EQ(5.11, acc2.getBalance());

}

TEST(AccountTest, transferIsUnsuccessfulDueToLowFund)
{
    Account acc1;
    acc1.deposit(100);

    Account acc2;
    
    EXPECT_THROW(acc1.transfer(acc2, 200), std::runtime_error);
}

int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}