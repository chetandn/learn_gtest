#include <gtest/gtest.h>
#include "BankAccount.hpp"
#include <iostream>

TEST(AccountTest, TestEmptyAccount)
{
    // Step 1: Arrange
    Account acc1;

    // Step 2: Act
    double balance = acc1.getBalance();

    // Step 3: Assert
    EXPECT_EQ(0, balance);

}

class AccountTestFixture : public testing::Test
{
public:
    void SetUp()
    {
        std::cout << "SetUp called\n";
        acc1.deposit(10.5);
    }

    void TearDown()
    {
        std::cout << "TearDown called\n";
    }

protected:
    Account acc1;
};

TEST_F(AccountTestFixture, TestDeposit)
{

    // Step 3: Assert
    EXPECT_EQ(10.5, acc1.getBalance());

}

TEST_F(AccountTestFixture, TestWithdraw)
{

    acc1.withdraw(5);

    // Step 3: Assert
    EXPECT_EQ(5.5, acc1.getBalance());

}

TEST_F(AccountTestFixture, TestWithdrawInsufficientFund)
{

    // Step 3: Assert
    EXPECT_THROW(acc1.withdraw(200.33), std::runtime_error);

}

TEST_F(AccountTestFixture, transferIsSucccessful)
{

    Account acc2;

    acc1.transfer(acc2, 5);

    EXPECT_EQ(5.5, acc1.getBalance());
    EXPECT_EQ(5, acc2.getBalance());

}

TEST_F(AccountTestFixture, transferIsUnsuccessfulDueToLowFund)
{

    Account acc2;
    
    EXPECT_THROW(acc1.transfer(acc2, 200), std::runtime_error);
}

int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}