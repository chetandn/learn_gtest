#include "BankAccount.hpp"
#include <stdexcept>

Account::Account() : mBalance{0}
{
}

void Account::deposit(double sum)
{
    mBalance = mBalance + sum;
}

void Account::withdraw(double sum)
{
    if(mBalance < sum)
    {
        throw std::runtime_error("Insufficient funds..!");
    }
    mBalance = mBalance - sum;
}

double Account::getBalance() const 
{
    return mBalance;
}

void Account::transfer(Account &to, double sum)
{
    withdraw(sum);
    to.deposit(sum);
}