#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <iostream>
#include "library.hpp"


class MockedClass : public MyClass
{
public:
    MockedClass() = default;
    MOCK_METHOD0(MyMethod, void());
};

TEST(TestSample, TestMock)
{
    MockedClass mc1;
    
    EXPECT_CALL(mc1, MyMethod()).Times(1);
    
    mc1.MyMethod();
}

int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}