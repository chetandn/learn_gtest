#include "IDatabaseConnection.hpp"
#include <iostream>

IDatabaseConnection::IDatabaseConnection(std::string serverAddress)
:
mServerAddress{serverAddress}
{
}

void IDatabaseConnection::connect()
{
    std::cout << "Connecting to database server : " << mServerAddress << std::endl;

    //if you want to call some method and test upon connect
    if (mOnConnect != nullptr)
    {
        std::cout << "Callback was set \n";
        mOnConnect();
    }

}

void IDatabaseConnection::disconnect()
{
    std::cout << "Disconnecting from database" << std::endl;
}

void IDatabaseConnection::SetOnConnect(Callback onConnect)
{
    mOnConnect = onConnect;
}