#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <iostream>

#include "Employee.hpp"
#include "IDatabaseConnection.hpp"
#include "EmployeeManager.hpp"

#include <functional>

using namespace testing;

class MockDatabaseConnection : public IDatabaseConnection
{
public:
    MockDatabaseConnection(std::string serverAddress);

    //commented out 
    // MOCK_METHOD0(connect, void()); 
    MOCK_METHOD0(disconnect, void());

    MOCK_CONST_METHOD1(getSalary, float(int));
    MOCK_METHOD2(updateSalary, void(int, float));

    MOCK_CONST_METHOD1(getSalariesRange, std::vector<Employee>(float));
    MOCK_CONST_METHOD2(getSalariesRange, std::vector<Employee>(float, float));

};

MockDatabaseConnection::MockDatabaseConnection(std::string serverAddress)
:
IDatabaseConnection(serverAddress)
{

}

// How can I set expectations on it?

// Solution 1:  is to wrap function inside a class
//              IDatabasConnection.hpp will have that class instead of Std Function Callback 
//              then you could mock that function in testrunner as other mocked function
// Cons - Modify your code, make calllback class which wraps around your function

// Solution 2: 
//GoogleTest has MockFuntion which coudl be used to mock the free function
// testing::MockFunction<void()> mockFunction;

// Instaed of mdc1.SetOnConnect(someCallbackFunctionToBeCalled);
// use mdc1.SetOnConnect(mockFunction.AsStdFunction);

// instead of EXPECT_CALL(Object, mockedFunction);, since there is no class
// EXPECT_CALL(newmockedFunctionName, Call()) 
// ex - EXPECT_CALL(mockFunction, Call()) 

void someCallbackFunctionToBeCalled()  
{
    std::cout << "Real Callback invoked\n"; // After mocking this method will never be called
}

TEST(TestEmployeeManager, CallbackTest)
{
    MockDatabaseConnection mdc1("dummyServerAddress");

    //mocked function w/o paramters
    testing::MockFunction<void()> mockFunction;
    mdc1.SetOnConnect(mockFunction.AsStdFunction())

    EXCEPT_CALL(mockFunction, Call());

    mdc1.connect();

/*
    mdc1.SetOnConnect(someCallbackFunctionToBeCalled);
    mdc1.connect();
 
Output before mock-
Connecting to database server : dummyServerAddress
Callback was set 
Real Callback invoked
 */


}


int main(int argc, char **argv){
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}